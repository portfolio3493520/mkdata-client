import { Telefone } from "./telefone.model";
import { TipoCliente } from "./tipo-cliente.model";

export class Cliente {
  id: number;
  nome: string;
  tipoCliente: TipoCliente;
  cpfCnpj: string;
  rgIe: string;
  dataCadastro: Date;
  ativo: boolean;
  telefones: Telefone[];
}
