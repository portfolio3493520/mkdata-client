import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Cliente } from '../models/cliente.model';
import { environment } from '../../environments/environment';

@Injectable()
export class ClienteRepository {
  private apiUrl = `${environment.apiUrl}/clientes`;

  constructor(private http: HttpClient) {}

  getClientes(): Observable<Cliente[]> {
    return this.http.get<Cliente[]>(this.apiUrl);
  }

  excluirCliente(id: number): Observable<any> {
    return this.http.delete(this.apiUrl + '/' + id);
  }

  cadastrarCliente(cliente: Cliente): Observable<Cliente> {
    return this.http.post<Cliente>(this.apiUrl, cliente);
  }

  atualizarCliente(cliente: Cliente, id: number): Observable<Cliente> {
    return this.http.put<Cliente>(this.apiUrl + '/' + id, cliente);
  }

  findById(id: number): Observable<Cliente> {
    return this.http.get<Cliente>(this.apiUrl + '/' + id);
  }

  findByNomeAndAtivo(filter: any) {
    return this.http.post<Cliente[]>(this.apiUrl + '/search', filter);
  }
}
