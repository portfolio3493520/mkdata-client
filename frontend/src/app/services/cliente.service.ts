import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Cliente } from '../models/cliente.model';
import { ClienteRepository } from '../repositories/cliente.repository';

@Injectable()
export class ClienteService {
  constructor(private clienteRepository: ClienteRepository) {}

  getClientes(): Observable<Cliente[]> {
    return this.clienteRepository.getClientes();
  }

  excluirCliente(id: number): Observable<any> {
    return this.clienteRepository.excluirCliente(id);
  }

  cadastrarCliente(cliente: Cliente, id: number = 0): Observable<Cliente> {
    if(id == 0) {
      return this.clienteRepository.cadastrarCliente(cliente);
    } else {
      return this.clienteRepository.atualizarCliente(cliente, id);
    }
  }

  findById(id: number): Observable<Cliente> {
    return this.clienteRepository.findById(id);
  }

  findByNomeAndAtivo(filter: any) {
    filter.ativo = filter.ativo != '' ? Number(filter.ativo) : filter.ativo;
    return this.clienteRepository.findByNomeAndAtivo(filter);
  }
}
