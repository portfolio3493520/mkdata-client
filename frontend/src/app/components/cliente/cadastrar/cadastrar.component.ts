import { Component, Inject, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef, MatSnackBar } from '@angular/material';
import { ClienteService } from '../../../services/cliente.service';
import { Cliente } from '../../../models/cliente.model';
import { TipoCliente } from '../../../models/tipo-cliente.model';

@Component({
  selector: 'app-cadastrar',
  templateUrl: './cadastrar.component.html',
  styleUrls: ['./cadastrar.component.css']
})
export class CadastrarComponent implements OnInit {

  clienteForm: FormGroup;
  submetido = false;
  textoBotaoAcao = 'Cadastrar';

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: { id: number },
    private clienteService: ClienteService,
    private formBuilder: FormBuilder,
    public dialogRef: MatDialogRef<CadastrarComponent>,
    private snackBar: MatSnackBar
  ) {}

  ngOnInit() {
    this.resetarClienteForm();
    if(this.data.id > 0) {
      this.textoBotaoAcao = 'Alterar';
      this.clienteService.findById(this.data.id).subscribe(cliente => {
        this.clienteForm.patchValue(cliente);
        this.clienteForm.patchValue({
          tipoCliente: TipoCliente[cliente.tipoCliente]
        })
        if(cliente.telefones.length > 0) {
          const novosTelefonesFormArray = cliente.telefones.map(telefone => this.formBuilder.group(telefone));
          this.clienteForm.setControl('telefones', this.formBuilder.array(novosTelefonesFormArray));
        }

      });
    }
  }

  resetarClienteForm() {
    this.clienteForm = this.formBuilder.group({
      id: [],
      nome: ['', Validators.required],
      tipoCliente: ['0', Validators.required],
      cpfCnpj: ['', Validators.required],
      rgIe: [''],
      ativo: ['1'],
      telefones: this.formBuilder.array([]),
    });
  }

  get telefones(): FormArray {
    return this.clienteForm.get('telefones') as FormArray;
  }

  adicionarTelefone(): void {
    this.telefones.push(this.criarTelefoneFormGroup());
  }

  removerTelefone(index: number): void {
    this.telefones.removeAt(index);
  }

  private criarTelefoneFormGroup(): FormGroup {
    return this.formBuilder.group({
      id: [],
      numero: ['', Validators.required],
    });
  }

  cadastrarCliente() {
    this.submetido = true;

    if (this.clienteForm.valid) {
      let cliente: any = this.clienteForm.getRawValue();
      cliente.ativo = Number(cliente.ativo);
      cliente.tipoCliente = TipoCliente[cliente.tipoCliente];
      if(!cliente.id) {
        cliente.id = 0;
      }
      this.clienteService.cadastrarCliente(cliente, cliente.id).subscribe(cliente => {
        this.dialogRef.close(true);
      }, error => {
        this.mostrarErro(error);
      })
    }
  }

  mostrarErro(error){
    this.snackBar.open(error.error.message ? error.error.message : error.message, 'Fechar', {
      duration: 5000,
      panelClass: 'error-snackbar'
    });
  }

  mascaraCpfCnpj() {
    const tipoCliente = TipoCliente[Number(this.clienteForm.get('tipoCliente').value)];
    if(tipoCliente === 'Jurídica') {
      return '00.000.000/0000-00';
    } else {
      return '000.000.000-00';
    }
  }

}
