import { NgModule } from '@angular/core';
import { ConsultarComponent } from './consultar/consultar.component';
import { CadastrarComponent } from './cadastrar/cadastrar.component';
import { ClienteService } from '../../services/cliente.service';
import { ClienteRepository } from '../../repositories/cliente.repository';
import { ConfirmationModalComponent } from '../confirmation-modal/confirmation-modal.component';
import { CommonModule } from '@angular/common';
import { MatButtonModule, MatCardModule, MatDialogModule, MatDividerModule, MatFormFieldModule, MatInputModule, MatRadioModule, MatSelectModule, MatSnackBar, MatSnackBarModule } from '@angular/material';
import { ClienteRoutingModule } from './cliente-routing.module';
import { MatIconModule } from '@angular/material/icon';
import { BreakpointObserver, MediaMatcher } from '@angular/cdk/layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxMaskModule } from 'ngx-mask';

@NgModule({
  imports: [
    CommonModule,
    ClienteRoutingModule,
    MatButtonModule,
    MatDialogModule,
    MatIconModule,
    MatSnackBarModule,
    MatFormFieldModule,
    MatIconModule,
    MatRadioModule,
    MatInputModule,
    MatSelectModule,
    MatCardModule,
    MatDividerModule,
    MatDialogModule,
    FormsModule,
    ReactiveFormsModule,
    NgxMaskModule.forRoot()
  ],
  declarations: [ConsultarComponent, CadastrarComponent, ConfirmationModalComponent],
  providers: [ClienteService, ClienteRepository, MatSnackBar, BreakpointObserver, MediaMatcher],
  entryComponents: [
    ConfirmationModalComponent
  ]
})
export class ClienteModule { }
