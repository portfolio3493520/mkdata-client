import { Component, OnInit } from '@angular/core';
import { Cliente } from '../../../models/cliente.model';
import { ClienteService } from '../../../services/cliente.service';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { ConfirmationModalComponent } from '../../confirmation-modal/confirmation-modal.component';
import { MatSnackBar } from '@angular/material';
import { CadastrarComponent } from '../cadastrar/cadastrar.component';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-consultar',
  templateUrl: './consultar.component.html',
  styleUrls: ['./consultar.component.css']
})
export class ConsultarComponent implements OnInit {
  clientes: Cliente[];
  filtroForm: FormGroup;

  constructor(
    private clienteService: ClienteService,
    private router: Router,
    private dialog: MatDialog,
    private snackBar: MatSnackBar,
    private formBuilder: FormBuilder
    ) {
      this.resetarFiltroForm();
  }

  ngOnInit(): void {
    this.carregarClientes();
  }

  resetarFiltroForm() {
    this.filtroForm = this.formBuilder.group({
      nome: [''],
      ativo: ['']
    });
  }

  carregarClientes() {
    this.clienteService.getClientes().subscribe(clientes => {
      this.clientes = clientes;
    }, error => {
      this.mostrarErro(error);
    });
  }

  editarCliente(id: number) {
    this.abrirModalCadastrar(id);
  }

  abrirConfirmacaoExclusao(id: number) {
    const dialogRef = this.dialog.open(ConfirmationModalComponent, {
      data: { message: 'Deseja realmente excluir este cliente?' }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.clienteService.excluirCliente(id).subscribe(clientes => {
          this.ngOnInit();
        }, error => {
          this.mostrarErro(error);
        });
      }
    });
  }

  abrirModalCadastrar(id: number = 0) {
    const dialogRef = this.dialog.open(CadastrarComponent, {
      data: { id: id }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.ngOnInit();
      }
    });
  }

  mostrarErro(error){
    this.snackBar.open(error.error.message ? error.error.message : error.message, 'Fechar', {
      duration: 5000,
      panelClass: 'error-snackbar'
    });
  }

  pesquisar(): void {
    this.clienteService.findByNomeAndAtivo(this.filtroForm.getRawValue()).subscribe(clientes => {
      this.clientes = clientes;
    })
  }

  limpar(): void {
    this.resetarFiltroForm();
    this.carregarClientes();
  }

  /**
   * Formata o telefone na máscara (00) 00000.0000
   * @param telefone
   * @returns
   */
  public formatarTelefone(telefone: string): string {
    return `(${telefone.substring(0, 2)}) ${telefone.substring(2, 7)}.${telefone.substring(7)}`;
  }

  /**
   * Formata o CPF ou CNPJ na máscara 000.000.000-00 ou 00.000.000/0000-00
   * @param cpfCnpj
   * @returns
   */
  public formatarCpfCnpj(cpfCnpj: string): string {
    if(cpfCnpj.length == 14) {
      return `${cpfCnpj.substring(0, 2)}.${cpfCnpj.substring(2, 5)}.${cpfCnpj.substring(5, 8)}/${cpfCnpj.substring(8, 12)}-${cpfCnpj.substring(12, 14)}`;
    } else {
      return `${cpfCnpj.substring(0, 3)}.${cpfCnpj.substring(3, 6)}.${cpfCnpj.substring(6, 9)}-${cpfCnpj.substring(9, 11)}`;
    }

  }

}
