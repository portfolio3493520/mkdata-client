import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ConsultarComponent } from './consultar/consultar.component';
import { CadastrarComponent } from './cadastrar/cadastrar.component';

const routes: Routes = [
  { path: '', redirectTo: 'cliente/consultar', pathMatch: 'full' },
  { path: 'cliente/consultar', component: ConsultarComponent },
  { path: 'cliente/cadastrar', component: CadastrarComponent },
  { path: 'cliente/cadastrar/:id', component: CadastrarComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ClienteRoutingModule { }
