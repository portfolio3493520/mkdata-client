import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { ClienteModule } from './components/cliente/cliente.module';

const routes: Routes = [
  { path: '', component: AppComponent },
  { path: 'cliente', loadChildren: () => Promise.resolve(ClienteModule)  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
