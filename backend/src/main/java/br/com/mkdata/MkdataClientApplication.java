package br.com.mkdata;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MkdataClientApplication {

	public static void main(String[] args) {
		SpringApplication.run(MkdataClientApplication.class, args);
	}

}
