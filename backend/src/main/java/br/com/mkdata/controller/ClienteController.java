package br.com.mkdata.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.mkdata.model.Cliente;
import br.com.mkdata.model.ClienteFilter;
import br.com.mkdata.service.ClienteService;

@RestController
@RequestMapping("/clientes")
public class ClienteController {

    private final ClienteService clienteService;

    @Autowired
    public ClienteController(ClienteService clienteService) {
        this.clienteService = clienteService;
    }

    @GetMapping
    public List<Cliente> listarClientes() {
        return clienteService.listarClientes();
    }

    @GetMapping("/{id}")
    public ResponseEntity<Cliente> consultarCliente(@PathVariable Long id) {
        Optional<Cliente> cliente = clienteService.consultarCliente(id);

        return cliente.map(ResponseEntity::ok).orElse(ResponseEntity.notFound().build());
    }

    @PostMapping
    public ResponseEntity<Cliente> cadastrarCliente(@RequestBody Cliente cliente) {
        Cliente novoCliente = clienteService.cadastrarCliente(cliente);
        if(novoCliente != null) {
        	return ResponseEntity.status(HttpStatus.CREATED).body(novoCliente);
        } else {
        	return ResponseEntity.badRequest().build();
        }
    }
    
    @PostMapping("/search")
    public ResponseEntity<List<Cliente>> findByNomeAndAtivo(@RequestBody ClienteFilter filter) {
        List<Cliente> clientes = clienteService.findByNomeAndAtivo(filter);
        return ResponseEntity.status(HttpStatus.CREATED).body(clientes);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Cliente> atualizarCliente(@PathVariable Long id, @RequestBody Cliente cliente) {
    	Optional<Cliente> clienteAtualizadoOptional = clienteService.atualizarCliente(id, cliente);

        if (clienteAtualizadoOptional.isPresent()) {
            return ResponseEntity.ok(clienteAtualizadoOptional.get());
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> removerCliente(@PathVariable Long id) {
    	boolean clienteRemovido = clienteService.removerCliente(id);

        if (clienteRemovido) {
            return ResponseEntity.noContent().build();
        } else {
            return ResponseEntity.notFound().build();
        }
    }
}
