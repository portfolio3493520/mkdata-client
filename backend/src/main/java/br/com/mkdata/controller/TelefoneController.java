package br.com.mkdata.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.mkdata.model.Telefone;
import br.com.mkdata.service.TelefoneService;

@RestController
@RequestMapping("/telefones")
public class TelefoneController {

    private final TelefoneService telefoneService;

    @Autowired
    public TelefoneController(TelefoneService telefoneService) {
        this.telefoneService = telefoneService;
    }

    @GetMapping
    public List<Telefone> listarTelefones() {
        return telefoneService.listarTelefones();
    }

    @GetMapping("/{id}")
    public ResponseEntity<Telefone> consultarTelefone(@PathVariable Long id) {
        Optional<Telefone> telefone = telefoneService.consultarTelefone(id);
        return telefone.map(ResponseEntity::ok).orElse(ResponseEntity.notFound().build());
    }

    @PostMapping
    public ResponseEntity<Telefone> cadastrarTelefone(@RequestBody Telefone telefone) {
        Telefone novoTelefone = telefoneService.cadastrarTelefone(telefone);
        return ResponseEntity.status(HttpStatus.CREATED).body(novoTelefone);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Telefone> atualizarTelefone(@PathVariable Long id, @RequestBody Telefone telefone) {
        Optional<Telefone> telefoneAtualizado = telefoneService.atualizarTelefone(id, telefone);

        return telefoneAtualizado.map(ResponseEntity::ok).orElse(ResponseEntity.notFound().build());
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> removerTelefone(@PathVariable Long id) {
    	telefoneService.removerTelefone(id);
        return ResponseEntity.noContent().build();
    }
}
