package br.com.mkdata.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.mkdata.model.Cliente;

public interface ClienteRepository extends JpaRepository<Cliente, Long> {
	
	public Optional<Cliente> findByCpfCnpj(String cpfCnpj);
	
	public List<Cliente> findByNomeContainingIgnoreCase(String nome);
	
	public List<Cliente> findByNomeContainingIgnoreCaseAndAtivo(String nome, Boolean ativo);
}
