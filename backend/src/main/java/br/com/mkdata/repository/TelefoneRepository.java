package br.com.mkdata.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.mkdata.model.Telefone;

public interface TelefoneRepository extends JpaRepository<Telefone, Long> {
}
