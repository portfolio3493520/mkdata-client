package br.com.mkdata.service;


import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.mkdata.model.Cliente;
import br.com.mkdata.model.ClienteFilter;
import br.com.mkdata.repository.ClienteRepository;
import br.com.mkdata.validator.Validator;

@Service
public class ClienteService {

    private final ClienteRepository clienteRepository;

    @Autowired
    public ClienteService(ClienteRepository clienteRepository) {
        this.clienteRepository = clienteRepository;
    }

    public List<Cliente> listarClientes() {
        return clienteRepository.findAll();
    }
    
    public List<Cliente> findByNomeAndAtivo(ClienteFilter filter) {
    	if(filter.getAtivo()==null) {
    		return clienteRepository.findByNomeContainingIgnoreCase(filter.getNome());
    	} else {
    		return clienteRepository.findByNomeContainingIgnoreCaseAndAtivo(filter.getNome(), filter.getAtivo());
    	}
    }
    
    public Optional<Cliente> consultarClientePorCpfCnpj(String cpfCnpj) {
        return clienteRepository.findByCpfCnpj(cpfCnpj);
    }

    public Optional<Cliente> consultarCliente(Long id) {
        return clienteRepository.findById(id);
    }

    public Cliente cadastrarCliente(Cliente cliente) {
    	if(Validator.isCPFOrCNPJ(cliente.getCpfCnpj(), cliente.getTipoCliente())) { //Verifica se CPF ou CNPJ é válido
    		cliente.setCpfCnpj(cliente.getCpfCnpj().replaceAll("\\D", ""));//Deixando apenas os caracteres numéricos
    		
	    	Optional<Cliente> clienteExistente = consultarClientePorCpfCnpj(cliente.getCpfCnpj());
	    	if (clienteExistente.isPresent()) {
	    		throw new IllegalArgumentException("Já existe um cliente cadastrado com o mesmo CPF ou CNPJ.");
	        } else {
	        	return clienteRepository.save(cliente);
	        }
    	} else {
    		throw new IllegalArgumentException("CPF ou CNPJ informado não é válido.");
    	}
    }

    public Optional<Cliente> atualizarCliente(Long id, Cliente cliente) {
    	if(Validator.isCPFOrCNPJ(cliente.getCpfCnpj(), cliente.getTipoCliente())) { //Verifica se CPF ou CNPJ é válido
    		cliente.setCpfCnpj(cliente.getCpfCnpj().replaceAll("\\D", ""));//Deixando apenas os caracteres numéricos
    		
	    	Optional<Cliente> clienteExistente = consultarClientePorCpfCnpj(cliente.getCpfCnpj()); //Consula pelo cpfCnpj do cliente
	        if (clienteExistente.isPresent() && clienteExistente.get().getId() != id) { //Caso encontre cliente com Id diferente, é pq cpfCnpj já existe
	        	throw new IllegalArgumentException("Já existe um cliente cadastrado com o mesmo CPF ou CNPJ.");
	        } else {
	        	Optional<Cliente> clienteExistenteById = clienteRepository.findById(id);//Caso contrário verifica se existe o ID que está pra ser atualizado
	        	if(clienteExistenteById.isPresent()) {
	        		cliente.setDataCadastro(clienteExistente.isPresent() ? clienteExistente.get().getDataCadastro() : LocalDateTime.now());//Mantém a data de cadastro do objeto original
		            return Optional.of(clienteRepository.save(cliente));
	        	}
	        	
	        }
	        return Optional.of(cliente);
    	} else {
    		throw new IllegalArgumentException("CPF ou CNPJ informado não é válido.");
    	}
    }

    public boolean removerCliente(Long id) {
        Optional<Cliente> clienteOptional = clienteRepository.findById(id);

        if (clienteOptional.isPresent()) {
            Cliente cliente = clienteOptional.get();
            clienteRepository.delete(cliente);
            return true;
        }

        return false;
    }
}
