package br.com.mkdata.service;


import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.mkdata.model.Telefone;
import br.com.mkdata.repository.TelefoneRepository;

@Service
public class TelefoneService {

    private final TelefoneRepository telefoneRepository;

    @Autowired
    public TelefoneService(TelefoneRepository telefoneRepository) {
        this.telefoneRepository = telefoneRepository;
    }

    public List<Telefone> listarTelefones() {
        return telefoneRepository.findAll();
    }

    public Optional<Telefone> consultarTelefone(Long id) {
        return telefoneRepository.findById(id);
    }

    public Telefone cadastrarTelefone(Telefone telefone) {
        return telefoneRepository.save(telefone);
    }

    public Optional<Telefone> atualizarTelefone(Long id, Telefone telefone) {
        Optional<Telefone> telefoneExistente = telefoneRepository.findById(id);

        if (telefoneExistente.isPresent()) {
        	telefoneExistente.get().setNumero(telefone.getNumero());
            telefoneRepository.save(telefoneExistente.get());
        }

        return telefoneExistente;
    }

    public void removerTelefone(Long id) {
    	telefoneRepository.deleteById(id);
    }
}
