package br.com.mkdata.controller;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.LocalDateTime;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import br.com.mkdata.model.Cliente;
import br.com.mkdata.model.TipoCliente;
import br.com.mkdata.service.ClienteService;

@ExtendWith(MockitoExtension.class)
public class ClienteControllerTest {

    @Mock
    private ClienteService clienteService;

    @InjectMocks
    private ClienteController clienteController;

    private MockMvc mockMvc;

    @BeforeEach
    public void setup() {
        mockMvc = MockMvcBuilders.standaloneSetup(clienteController).build();
    }

    @Test
    public void consultarCliente_Existente_DeveRetornarCliente() throws Exception {
        Long clienteId = 1L;
        String clienteNome = "Cliente Teste";
        TipoCliente tipoCliente = TipoCliente.Física;
        String cpfCnpj = "83960058004";
        LocalDateTime dataCadastro = LocalDateTime.now();
        boolean ativo = true;

        Cliente cliente = new Cliente();
        cliente.setId(clienteId);
        cliente.setNome(clienteNome);
        cliente.setTipoCliente(tipoCliente);
        cliente.setCpfCnpj(cpfCnpj);
        cliente.setDataCadastro(dataCadastro);
        cliente.setAtivo(ativo);

        when(clienteService.consultarCliente(clienteId)).thenReturn(Optional.of(cliente));

        mockMvc.perform(get("/clientes/{id}", clienteId))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id").value(clienteId))
                .andExpect(jsonPath("$.nome").value(clienteNome))
                .andExpect(jsonPath("$.tipoCliente").value(tipoCliente.name()))
                .andExpect(jsonPath("$.cpfCnpj").value(cpfCnpj))
                .andExpect(jsonPath("$.ativo").value(ativo));
    }

    @Test
    public void consultarCliente_Inexistente_DeveRetornarNotFound() throws Exception {
        Long clienteId = 2L;

        when(clienteService.consultarCliente(clienteId)).thenReturn(Optional.empty());

        mockMvc.perform(get("/clientes/{id}", clienteId))
                .andExpect(status().isNotFound());
    }

    @Test
    public void cadastrarCliente_DeveRetornarClienteCriado() throws Exception {
        Long clienteId = 1L;
        String clienteNome = "Cliente Teste";
        TipoCliente tipoCliente = TipoCliente.Física;
        String cpfCnpj = "83960058004";
        boolean ativo = true;

        Cliente cliente = new Cliente();
        cliente.setId(clienteId);
        cliente.setNome(clienteNome);
        cliente.setTipoCliente(tipoCliente);
        cliente.setCpfCnpj(cpfCnpj);
        cliente.setAtivo(ativo);

        when(clienteService.cadastrarCliente(any(Cliente.class))).thenReturn(cliente);

        mockMvc.perform(post("/clientes")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"nome\": \"Cliente Teste\", \"tipoCliente\": 0, \"cpfCnpj\": \"83960058004\", \"ativo\": true}"))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id").value(clienteId))
                .andExpect(jsonPath("$.nome").value(clienteNome))
                .andExpect(jsonPath("$.tipoCliente").value(tipoCliente.name()))
                .andExpect(jsonPath("$.cpfCnpj").value(cpfCnpj))
                .andExpect(jsonPath("$.ativo").value(ativo));
    }
    
    @Test
    public void cadastrarCliente_SemNome_DeveRetornarBadRequest() throws Exception {
        mockMvc.perform(post("/clientes")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"nome\": \"\"}"))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void atualizarCliente_Existente_DeveRetornarClienteAtualizado() throws Exception {
        Long clienteId = 1L;
        String novoNome = "Novo Nome";
        TipoCliente novoTipoCliente = TipoCliente.Jurídica;
        String novoCpfCnpj = "28292052000106";
        boolean novoAtivo = false;

        Cliente clienteAtualizado = new Cliente();
        clienteAtualizado.setId(clienteId);
        clienteAtualizado.setNome(novoNome);
        clienteAtualizado.setTipoCliente(novoTipoCliente);
        clienteAtualizado.setCpfCnpj(novoCpfCnpj);
        clienteAtualizado.setAtivo(novoAtivo);

        when(clienteService.atualizarCliente(eq(clienteId), any(Cliente.class))).thenReturn(Optional.of(clienteAtualizado));

        mockMvc.perform(put("/clientes/{id}", clienteId)
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"nome\": \"Novo Nome\", \"tipoCliente\": 1, \"cpfCnpj\": \"28292052000106\", \"ativo\": false}"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id").value(clienteId))
                .andExpect(jsonPath("$.nome").value(novoNome))
                .andExpect(jsonPath("$.tipoCliente").value(novoTipoCliente.name()))
                .andExpect(jsonPath("$.cpfCnpj").value(novoCpfCnpj))
                .andExpect(jsonPath("$.ativo").value(novoAtivo));
    }
    
    @Test
    public void atualizarCliente_Inexistente_DeveRetornarNotFound() throws Exception {
        Long clienteId = 1L;

        when(clienteService.atualizarCliente(eq(clienteId), any(Cliente.class))).thenReturn(Optional.empty());

        mockMvc.perform(put("/clientes/{id}", clienteId)
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"nome\": \"Novo Nome\"}"))
                .andExpect(status().isNotFound());
    }

}