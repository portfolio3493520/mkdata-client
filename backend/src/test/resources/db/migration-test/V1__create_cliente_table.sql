CREATE TABLE cliente (
    id_cliente BIGINT AUTO_INCREMENT PRIMARY KEY,
    nome VARCHAR(255) NOT NULL,
	tipo_cliente integer NOT NULL,
	cpf_cnpj VARCHAR(255) NOT NULL,
	rg_ie VARCHAR (255),
	data_cadastro DATETIME DEFAULT CURRENT_TIMESTAMP,
	ativo boolean DEFAULT TRUE
);

CREATE TABLE telefone (
	id_telefone BIGINT AUTO_INCREMENT PRIMARY KEY,
	numero VARCHAR(255) NOT NULL,
	id_cliente INTEGER NOT NULL,
	FOREIGN KEY (id_cliente) REFERENCES cliente(id_cliente)
);

INSERT INTO cliente values(1, 'Cliente Teste', 0, '47346941098',null,now(),true);
INSERT INTO telefone values(1, '85999887766',1);
INSERT INTO telefone values(2, '85955443322',1);