# mkdata-client



## Início

Esse projeto contempla um cadastro de cliente simples utilizando Backend em Java com Frontend em Angular

## Tecnologias utilizadas no Backend

```
- Spring Boot 3.0.6
- Java 17
- Flyway
- H2 Database
- JUnit
- Mockito
- Maven 3.6.3
```

## Tecnologias utilizadas no Frontend

```
- Angular 5.2.0
- Angular Material
- Ngx-Mask
- Node 14.15
```

## Preparando o ambiente

```
- Certifique-se de ter o Java 17 instalado e a variável de ambiente JAVA_HOME apontando para o diretório da instalação.
- Certifique-se de ter o Maven instalado e com a variável M2_HOME apontando para o diretório de instalação.
- Certifique-se de ter o Node 14.15 instalado.
```

## Subindo o Backend

```
- Acesse o diretório backend do projeto via prompt ou terminal
- mvn clean dependency:resolve -U
- mvn clean install -U
- mvn clean package -DskipTests
- mvn spring-boot:run
```
Feito o passo a passo acima o backend já estará rodando em **http://localhost:8080**

## Subindo o Frontend

```
- Acesse o diretório frontend do projeto via prompt ou terminal
- npm install
- ng s
```
Feito o passo a passo acima o frontend já estará rodando em **http://localhost:4200.**
Esse é o endereço para acessar a aplicação consumir a API de Cliente